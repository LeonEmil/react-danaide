import * as actions from './actionTypes' 

const addCurrentVideo = (id) => {
    return {
        type: actions.ADD_CURRENT_VIDEO,
        id: id
    }
}

export {
    addCurrentVideo
}