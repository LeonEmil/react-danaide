import * as actions from './actionTypes'

const initialState = {
    currentVideo: '00003288-b499-11ea-a97b-dae54befdcb1'
}

const reducer = (state = initialState, action) => {
    switch (action.type) {       
        case actions.ADD_CURRENT_VIDEO:
            console.log(action)
            return {
                ...state,
                currentVideo: action.id
            }    
    
        default:
            return state
    }
}

export default reducer
