import React, { useState, useRef } from 'react'
import { connect } from 'react-redux'
import { addCurrentVideo } from './../redux/actionCreators'
import Menu from './Menu'
import axios from 'axios'
import { Redirect } from 'react-router-dom'

const Channels = ({channels, playlist, playlistLength, addCurrentVideo}) => {

    const [redirect, setRedirect] = useState(false)
    const buttonRef = useRef()

    // const onClick = (action, id, message, ref) => {
    //     console.log(buttonRef.current) 
    //     alert("Video agregado a la playlist")
    //     axios.get(`http://develop.danaide.com.ar/test/api/playlist/add/${channel.id}`)
    // }

    return redirect ? <Redirect to="/playlist"/> : (
        <>
            <Menu />
            <div className="channels">
                <ul className="channels__list">
                    {
                        channels.map((channel, key) => (
                            <li className="channels__item" key={key + playlistLength}>
                                <span className="channels__item-child">{channel.name}</span>
                                <button className="channels__item-child" onClick={() => {addCurrentVideo(channel.id); setRedirect(true)}}>Ver video</button>
                                {
                                    playlist ?
                                        playlist.map(el => el.id).includes(channel.id) ? 
                                            <button className="channels__item-child" onClick={() => {
                                                axios.get(`http://develop.danaide.com.ar/test/api/playlist/remove/${channel.id}`)
                                                alert("Video eliminado de la playlist")
                                                // onClick('http://develop.danaide.com.ar/test/api/playlist/remove/', channel.id, "Video eliminado de la playlist", useRef)
                                            }}
                                            >Quitar de la playlist</button>
                                        :
                                            <button className="channels__item-child" onClick={() => {
                                                axios.get(`http://develop.danaide.com.ar/test/api/playlist/add/${channel.id}`)
                                                alert("Video agregado a la playlist")
                                            }}
                                            >Agregar a la playlist</button>
                                    : null
                                }
                            </li>
                        ))
                    }
                </ul>
            </div>
        </>
    )
}

const mapDispatchToProps = (dispatch) => {
    return {
        addCurrentVideo: (id) => {dispatch(addCurrentVideo(id))}
    }
}

export default connect(null, mapDispatchToProps)(Channels)
