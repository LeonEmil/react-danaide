import React from 'react'
import Menu from './Menu'
import { connect } from 'react-redux'
import axios from 'axios'
import { addCurrentVideo } from './../redux/actionCreators'

const Playlist = ({playlist, playlistLength, currentVideo, addCurrentVideo}) => {

    return (
        <>
            <Menu />
            <video className="video" controls >
                <source src={`http://develop.danaide.com.ar/test/api/video/${currentVideo}.mp4`}  type="video/mp4"/>
                Your browser does not support the video tag.
            </video>
            <nav className="playlist">
                <ul className="playlist__list">
                    {
                        playlist.map((item, key) => (
                            <li key={key + playlistLength} className="playlist__item">
                                <span className="playlist__item-child">{item.name}</span>
                                <button className="playlist__item-child" onClick={() => {addCurrentVideo(item.id)}}>Ver video</button>
                                <button className="playlist__item-child" onClick={() => {axios.get(`http://develop.danaide.com.ar/test/api/playlist/remove/${item.id}`)}}>Quitar de la playlist</button>
                            </li>
                        ))
                    }
                </ul>
            </nav>
        </>
    )
}

const mapStateToProps = (state) => {
    return {
        currentVideo: state.currentVideo
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        addCurrentVideo: (id) => { dispatch(addCurrentVideo(id)) }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Playlist)
