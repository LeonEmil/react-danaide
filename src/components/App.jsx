import React, { useState, useEffect } from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'

import Channels from './Channels'
import Playlist from './Playlist'
import NotFound from './NotFound'
import axios from 'axios'
import { Provider } from 'react-redux'
import store from './../redux/store'
import './../sass/style.scss'

function App() {

  const [channels, setChannels] = useState([]) 
  const [playlist, setPlayList] = useState([])
  const [playlistLength, setPlayListLength] = useState(0)
  const [event, setEvent] = useState({
    text: "",
    type: "",
    img: "",
  })

  useEffect(() => {
      const interval = setInterval(() => {
        axios.all([
          axios.get('http://develop.danaide.com.ar/test/api/channels'), 
          axios.get('http://develop.danaide.com.ar/test/api/playlist'),
          axios.get('http://develop.danaide.com.ar/test/api/event')
        ])
        .then(response => {
          setChannels(response[0].data)
          setPlayList(response[1].data)
          setPlayListLength(response[1].data.length)
          if(response[2].data.length > 0){
            axios.get(`http://develop.danaide.com.ar/test/api/event/image/${response[2].data[0].id}`).then(image => {
              setEvent({
                text: response[2].data[0].text,
                type: response[2].data[0].type,
                img: `http://develop.danaide.com.ar/test/api/event/image/${response[2].data[0].id}`
              })
            })
            .catch(error => {
              console.log(error)
            })
          } 
        })
        .catch(error => {
          console.log(error)
        })
      }, 1000);
      return () => clearInterval(interval); 
    })

  return (
    <>
    <div className="event">
      {
        event.img ? 
          <>
            <img src={event.img} alt={``} className="event__img"/>
            <span className="event__text">{event.text}: {event.type}</span>
          </>
        :
          null
      }
    </div>
    <Provider store={store}>
      <Router basename={process.env.PUBLIC_URL}>
        <Switch>
          <Route exact path="/">
            <Channels channels={channels} playlist={playlist} playlistLength={playlistLength}/>
          </Route>
          <Route path="/playlist">
            <Playlist playlist={playlist} playlistLength={playlistLength}/>
          </Route>
          <Route component={() => { return <NotFound /> }}>
          </Route>
        </Switch>
      </Router>
    </Provider>
    </>
  );
}

export default App;
